import java.util.Random;

/**
 * Created by nikita on 20/05/16.
 */
public class CreateArray {

    public int[] slowpoke;

    public void sayMyName() {
        System.out.println("Your name is not important.");
    }

    public void fillArray(int size){
        slowpoke = new int[size];
        Random rnd = new Random();

        for (int i=0;i<size;i++){
            slowpoke[i]=rnd.nextInt(100);
        }
    }

    public void showArray(int[] array){
        for (int i=0;i<array.length;i++){
            System.out.println(array[i]);
        }
    }

    public int[] newArr(int size){
        int newFilledArr[] = new int[size];
        Random rnd = new Random();

        for (int i=0;i<size;i++){
            newFilledArr[i]=rnd.nextInt(100);
        }
        return newFilledArr;
    }

    public int[] increaseArr(int[] array,int newitem){
        int increasedArr[] = new int[array.length+1];
        for (int i=0;i<array.length;i++) {
            increasedArr[i] = array[i];
        }
        increasedArr[array.length] = newitem;
        return increasedArr;
    }
}
