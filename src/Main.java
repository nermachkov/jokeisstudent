import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        int dre[];
        int dreSize = 5;
        dre = new int[dreSize];
        System.out.println("Hello World!");

//        CreateArray kamaz = new CreateArray();
//        kamaz.sayMyName();
//        dre = kamaz.newArr(dreSize);
//        kamaz.showArray(dre);
//        System.out.println("...increasing size");
//        dre = kamaz.increaseArr(dre,333);
//        kamaz.showArray(dre);
//        System.out.println("Ready!");

//  TODO: wait for user input every time
//  TODO: clear screen before user input
        MassivHandler mas = new MassivHandler();
        mas.create();
        System.out.println("1 unit");
        mas.show();
        mas.create(5);
        System.out.println("add 33");
        mas.add(33);
        mas.add(10,51);
        System.out.println("5 units");
        mas.show();
        System.out.println("add 1,34");
        mas.add(1,34);
        mas.show();
        System.out.println("add 0,99");
        mas.add(0,99);
        mas.show();
        System.out.println("add 5,55");
        mas.add(5,55);
        mas.show();
        System.out.println("rm 1");
        mas.removeInd(1);
        mas.show();
        System.out.println("find 1");
        System.out.println(mas.getIndex(1));
        System.out.println("find 33");
        System.out.println(mas.getIndex(33));
        System.out.println("Bubble");
        mas.sortBubble();
        mas.show();

    }
}
